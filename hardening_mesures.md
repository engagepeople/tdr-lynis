`https://elatov.github.io/2017/06/install-lynis-and-fix-some-suggestions/#add-a-legal-banner-to-etcissue-to-warn-unauthorized-users-bann-7126`
# Enable process accounting ACCT-9622, ..6
`sudo apt-get install acct`
`sudo systemctl enable acct.service`
`sudo systemctl start acct.service`

# Enable auditd to collect audit information ACCT-9628
`https://linuxhint.com/auditd_linux_tutorial/`
`sudo apt-get install auditd`
`sudo systemctl enable auditd`
`sudo systemctl start auditd`
# Audit daemon is enabled with an empty ruleset. Disable the daemon or define rules ACCT-9630
`sudo systemctl stop auditd`
`sudo systemctl disable auditd`
`or 
`substitute /etc/audit/rules.d/audit.rules`

# Install a PAM module for password strength testing like pam_cracklib or pam_passwdqc AUTH-9262
`apt-get install libpam-cracklib`

# Configure min/maximum password age in /etc/login.defs AUTH-9286
`done: in pam.yml`

# Add a legal banner to /etc/issue, to warn unauthorized users BANN-7126, BANN-7130
`- name: A legal banner`
`  ansible.builtin.lineinfile:`
`    path: /etc/issue`
`    insertafter: EOF`
`    line: 'Unauthorized access to this machine is prohibited Press <Ctrl-D> if you are not an authorized user'`
`заменить "#Banner no" на "Banner /etc/issue" в /etc/ssh/sshd_config`
`sudo systemctl reload sshd`

# Set a password on GRUB boot loader to prevent altering boot configuration (e.g. boot in single user mode without password) BOOT-5122
`password: ubuntu`
`hash:`
`"grub.pbkdf2.sha512.10000.AD4094349B51DF17BFAC5747BA9092454AFD7444AE75EBF2F113CC12843B03D9ABF518994B9BC791B07A1D3C347B4D87CFC3BF3D968C9A49060EDC7BCEE27578.E260FF27A4970DE8D34757D2DCCF3D887883D63976FE5C4778EEDF4A0A8B911D84CD10AB44AE7932573BBDBF489A6292376F53967327CF6D11E4918B1FB21651"`

# Check files in /tmp which are older than 90 days FILE-6354
`sudo find /tmp -mtime +89 -delete`

# Double check the permissions of home directories as some might be not strict enough HOME-9304
`getent passwd | grep -vE nologin | grep home | while IFS=: read -r name password uid gid gecos home shell; do`
  `echo $home`
  `chmod g-w,o-rwx $home`
`done`
`"chown -> 750"`

# Install a file integrity tool to monitor changes to critical and sensitive files FINT-4350
`sudo apt install aide`
!/raidarray/.*

# Harden compilers like restricting access to root user only HRDN-7222
`sudo chmod o-rx /usr/bin/gcc`
`sudo chmod o-rx /usr/bin/g++`
`gmu-as`

# Harden the system by installing at least one malware scanner, to perform periodic file system scans HRDN-7230
`sophos`

# If not required, consider explicit disabling of core dump in /etc/security/limits.conf file KRNL-5820
`append lines:`
`'* hard core 0'`
`'* soft core 0'`


# Determine if protocol 'dccp, rds, sctp, tipc' is really needed on this system NETW-3200
`etc/modprobe.d/blacklist-rare-network.conf`
`install dccp /bin/true`
`install sctp /bin/true`
`install rds /bin/true`
`install tipc /bin/true`

# Purge old/removed packages with aptitude purge or dpkg --purge command. This will cleanup old configuration files, cron jobs and startup scripts PKGS-7346
`sudo apt clean && sudo apt autoclean && sudo apt autoremove`
`PKGS-7346|Purge old/removed packages`

# Install debsums utility for the verification of packages with known good database. PKGS-7370
`sudo apt-get install -y debsums`

# Update your system with apt-get update, apt-get upgrade, apt-get dist-upgrade and/or unattended-upgrades PKGS-7392
`sudo apt update -y`
`sudo apt upgrade -y`
`sudo apt dist-upgrade -y`
`sudo apt autoremove -y`
`sudo apt clean -y`
`sudo apt autoclean -y`

# Consider hardening SSH configuration SSH-7408
`PermitRootLogin prohibit-password`
`X11Forwarding no`
`AllowAgentForwarding no`
`UseDNS yes`

# Determine if automation tools are present for system management TOOL-5002
`sudo ln -s /usr/share/bash-completion/completions/puppet /usr/bin/puppet`

# Disable drivers like USB storage when not used, to prevent unauthorized storage or data theft USB-1000
`- name: Check that the file blacklist.conf exists`
`  stat:`
`    path: /etc/modprobe.d/blacklist.conf`
`  register: blacklist_conf`

`- name: Disable USB storage`
`  ansible.builtin.lineinfile:`
`    path: /etc/modprobe.d/blacklist.conf`
`    insertafter: EOF`
`    line: 'blacklist usb-storage'`
`  when: blacklist_conf.stat.exists`

# Change the allow_url_fopen line to: allow_url_fopen = Off, to disable downloads via PHP PHP-2376
/etc/php/7.4/cli/php.ini
    allow_url_fopen = Off

# One or more sysctl values differ from the scan profile and could be tweaked KRNL-6000
1. the file with default state created and copied to work dir (sysctl-1 -> ~/lynis/sysctl_conf E)
`sudo sysctl -a > /tmp/sysctl-defaults.conf`
2. corrections in default made using lynis report info KRNL-6000
3. the file uploaded from work dir to remote and corrections inserted into system (sysctl-2 ~/lynis/sysctl_conf F -> remote)
`sudo sysctl --system`
  `/etc/sysctl.d/80-lynis.conf`
  `/run/sysctl.d/*.conf`
  `/usr/lib/sysctl.d/00-system.conf`
at an end of corrections:
   `service procps start`

# Enable sysstat to collect accounting (disabled) ACCT-9626
`https://www.crybit.com/sysstat-sar-on-ubuntu-debian/`
`apt install sysstat`

# Check what deleted files are still in use and why LOGG-2190
lsof | grep -e COMMAND -e '\(deleted\)'
analysis shows that this is normal behavior for the services that uses these files

# Disable the 'VRFY' command MAIL-8820
`http://www.securityspace.com/smysecure/catid.html?id=1.3.6.1.4.1.25623.1.0.100072`
`For postfix add 'disable_vrfy_command=yes in main.cf.`

# Consider restricting file permissions FILE-7524
chmod 600 /etc/crontab /etc/cron.d /etc/cron.daily /etc/cron.hourly /etc/cron.weekly /etc/cron.weekly /etc/ssh/sshd_config /etc/cron.monthly 
lynis audit system --tests-from-group "file_permissions"

# Check connection to this nameserver and make sure no outbound DNS queries are blocked (port 53 UDP and TCP) NETW-2704
`https://stackoverflow.com/questions/10750156/host-parse-of-etc-resolv-conf-failed`

# Consider hardening system services BOOT-5264
#Sand box
IPAddressDeny=any
LockPersonality=yes
MemoryDenyWriteExecute=yes
NoNewPrivileges=yes
PrivateDevices=yes
PrivateTmp=yes
ProtectSystem=strict
ProtectControlGroups=yes
ProtectHome=yes
ProtectHostname=yes
ProtectKernelTunables=yes
ProtectKernelModules=yes
ProtectKernelLogs=yes
RestrictAddressFamilies=AF_UNIX AF_NETLINK
RestrictNamespaces=yes
RestrictRealtime=yes
RestrictSUIDSGID=yes
SystemCallArchitectures=native
Type=notify
User=systemd-resolve
