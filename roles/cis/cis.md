Linux Disable Mounting of Uncommon Filesystem: (1.1.1.1 - 1.1.1.8)
`https://www.codeleading.com/article/37075519709/`
1.1.1.1 Run the following commands and verify the output is as indicated:
    # modprobe -n -v cramfs | grep -v mtd
    install /bin/true
    # lsmod | grep cramfs !!!
    <No output>
1.1.1.2 - 7 freevxfs, jffs2, hfs, hfsplus, squashfs, udf
1.1.1.8 IF FAT filesystem is required, run the following commond to see filesytem is only used where approproate:
    # grep -E -i '\svfat\s' /etc/fstab
    Run the following commands and verify the output is as indicated: 
    # modprobe --showconfig | grep vfat 
    install vfat /bin/true 
    # lsmod | grep vfat 
    <No output>   
1.1.21 Run the following command to set the sticky bit on all world writable directories:
# df --local -P | awk '{if (NR!=1) print $6}' | xargs -I '{}' find '{}' -xdev -type d \( -perm -0002 -a ! -perm -1000 \) 2>/dev/null | xargs -I '{}' chmod a+t '{}'