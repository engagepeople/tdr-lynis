#!/bin/sh
getent passwd | grep -vE nologin | grep home | while IFS=: read -r name password uid gid gecos home shell; do
  echo $home
  chmod g-w,o-rwx $home
done

if [ -e /var/lib/buildkite-agent ]
then
chmod g-w,o-rwx /var/lib/buildkite-agent
fi
if [ -e /var/lib/snmp ]
then
chmod g-w,o-rwx /var/lib/snmp
fi