#!/bin/sh
getent passwd | grep -vE nologin | grep home | while IFS=: read -r name password uid gid gecos home shell; do
  ISLOCKED=`passwd -S $name | grep ' L ' | cut -d ' ' -f1`
  if  [ -z "$ISLOCKED" ]
  then
    echo "not locked $name"
  else
    echo "locked $name"
    deluser --remove-home $name 
  fi
done
